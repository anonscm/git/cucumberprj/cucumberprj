package de.tarent.cucumber.datamanager;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;

@Ignore("Only for manuell use")
public class TestUtils {

	public static <T> List<T> toList(T...elements){
		List<T> result = new ArrayList<T>();
		
		for(T element : elements){
			result.add(element);
		}
		
		return result;
	}
}
