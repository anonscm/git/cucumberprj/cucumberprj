package de.tarent.cucumber.datamanager.functionality;

/**
 * Alle erweiterten Funktionalitäten werden durch
 * dieses diese Klasse ausgewertet.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public interface FunctionalityInterpreter {

	/**
	 * Wenn eine Funktionalität ausgewertet werden soll,
	 * wird diese Methode aufgerufen.
	 * 
	 * @param functionalityName Name der Funktionalität
	 * @param args Argumente, die übergeben wurden.
	 * @return Auswertung
	 */
	public String interpret(String functionalityName, String[] args) throws FunctionalityException;
}
