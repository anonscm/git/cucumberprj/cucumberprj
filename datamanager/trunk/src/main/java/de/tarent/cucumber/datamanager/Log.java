package de.tarent.cucumber.datamanager;

public class Log {

	public static void log(String message){
		System.out.println("[LOG]" + message);
	}
	
	public static void error(String message){
		System.err.println("[ERROR] " + message);
	}
	
	public static void error(String message, Throwable e){
		System.err.println("[ERROR] " + message);
		e.printStackTrace();
	}
	
	public static void warn(String message){
		System.out.println("[WARN] " + message);
	}
	
	public static void info(String message){
		System.out.println("[INFO] " + message);
	}
}
