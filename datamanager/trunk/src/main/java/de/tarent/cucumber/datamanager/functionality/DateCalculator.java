package de.tarent.cucumber.datamanager.functionality;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Dieser Interpreter rechnet mit Daten. Dazu kann
 * man entweder mit dem aktuellen Zeitpunkt oder mit
 * einem festen Zeitpunkt rechnen.
 * Beispiele:
 * <ul>
 * 	<li>&lt;name&gt; +1d &lt;date-format&gt; -&gt; Aktuelles Datum plus einen Tag</li>
 * </ul>
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class DateCalculator implements FunctionalityInterpreter {

	public String interpret(String functionalityName, String[] args) throws FunctionalityException{
		if(args.length < 2){
			throw new FunctionalityException(FunctionalityException.MESSAGE_INVALID_PARAMETER);
		}
		
		Date usedDate = new Date();
		String outputFmt = args[1];
		int amount = getNumber(args[0]);
		int modus = getModus(args[0]);
		
		if(!add(args[0])){
			amount *= -1;	//vorzeichen ändern!
		}
		
		SimpleDateFormat fmt = new SimpleDateFormat(outputFmt);
		Calendar c = new GregorianCalendar();
		c.setTime(usedDate);
		c.add(modus, amount);
		
		return fmt.format(c.getTime());
	}

	int getNumber(String arg){
		return Integer.parseInt(arg.substring(1, arg.length() - 1));
	}
	
	boolean add(String arg){
		return arg.charAt(0) != '-';
	}
	
	int getModus(String arg){
		char lastChar = arg.charAt(arg.length() - 1);
		
		switch(lastChar){
			case 'm': return Calendar.MINUTE;
			case 'M': return Calendar.MONTH;
			case 's': return Calendar.SECOND;
			case 'h': return Calendar.HOUR_OF_DAY;
			case 'y': return Calendar.YEAR;
			default: return Calendar.DAY_OF_YEAR;
		}
	}
}
