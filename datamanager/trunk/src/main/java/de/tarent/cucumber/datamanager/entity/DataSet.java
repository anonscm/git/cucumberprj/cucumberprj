package de.tarent.cucumber.datamanager.entity;

import java.util.List;

public class DataSet extends Entity {
	private List<Field> values;

	public List<Field> getValues() {
		return values;
	}

	public void setValues(List<Field> values) {
		this.values = values;
	}
}
