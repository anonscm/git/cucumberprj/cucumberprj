package de.tarent.cucumber.datamanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Hilfsklasse für den DataManager. Beinhaltet diverse nützliche Funktionen.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class Utils {

	/**
	 * Liest den gesamten Dateiinhalt in ein String und liefert diesen.
	 * 
	 * @param path
	 *            Pfad zur Datei.
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String path) throws IOException {
		BufferedReader is = new BufferedReader(new FileReader(new File(path)));

		StringBuilder stringBuilder = new StringBuilder();
		String line = null;

		while ((line = is.readLine()) != null) {
			// entferne Kommentare
			if (!line.trim().startsWith("#")) {
				stringBuilder.append(line);
			}
		}

		is.close();
		return stringBuilder.toString();
	}

	/**
	 * Liefert alle Sub-Strukturen, die in dem angegeben String sein könnten.
	 * (Mindestens eine Struktur)
	 * 
	 * @param toSplit
	 * @return
	 */
	public static List<String> splitStructures(String toSplit) {
		List<String> result = new ArrayList<String>();

		String[] splited = toSplit.split("\\}\\s*\\{");
		if (splited.length > 1) {
			for (int i = 0; i < splited.length; i++) {
				// gesplitete Zeichen wieder anfügen
				if (i > 0) {
					splited[i] = "{" + splited[i];
				}

				if (i + 1 != splited.length) {
					splited[i] += "}";
				}

				result.add(splited[i]);
			}
		} else {
			result.add(toSplit);
		}

		return result;
	}

	public static Map<String, Object> jsonToMap(JsonObject jObject) {
		Map<String, Object> result = new HashMap<String, Object>();

		for (Entry<String, JsonElement> entry : jObject.entrySet()) {
			JsonElement value = entry.getValue();

			if (value instanceof JsonPrimitive) {
				result.put(entry.getKey(), value.getAsString());
			} else if (value instanceof JsonArray) {
				// !rekursion!
				result.put(entry.getKey(), jsonToList(value.getAsJsonArray()));
			} else if (value instanceof JsonObject) {
				result.put(entry.getKey(), jsonToMap(value.getAsJsonObject()));
			} else {
				result.put(entry.getKey(), value);
			}
		}

		return result;
	}

	public static List<Object> jsonToList(JsonArray jArray) {
		List<Object> result = new ArrayList<Object>();

		Iterator<JsonElement> iter = jArray.iterator();
		while (iter.hasNext()) {
			JsonElement element = iter.next();

			if (element instanceof JsonPrimitive) {
				result.add(element.getAsString());
			} else if (element instanceof JsonArray) {
				// !rekursion!
				result.add(jsonToList(element.getAsJsonArray()));
			} else if (element instanceof JsonObject) {
				result.add(jsonToMap(element.getAsJsonObject()));
			} else {
				result.add(element);
			}
		}

		return result;
	}

	/**
	 * Liest aus dem Klassenpfad die Dateien aus, die die angegebene Endung
	 * besitzen.
	 * 
	 * @param postfix
	 *            Endung, die die gewünschten Dateien besitzen müssen
	 * @return
	 * @throws IOException 
	 */
	public static List<String> getFilesFromClassPathWithPostfix(String postfix) throws IOException {
		List<String> result = new ArrayList<String>();
		
		Enumeration<URL> resources = Utils.class.getClassLoader().getResources("");
			
		while (resources.hasMoreElements()) {
			URL url = resources.nextElement();
			if (url.getProtocol().equals("file")) {
				
				if(url.getPath().endsWith("/")){
					//this is a directory
					for(String filePath : scanDirectory(new File(url.getPath()))){
						if(filePath.endsWith(postfix)){
							//... a json-file
							result.add(filePath);
						}
					}
				}else{
					//this is a file
					if(url.getPath().endsWith(postfix)){
						//... a json-file
						result.add(url.getPath());
					}
				}
			}
		}
		
		return result;
	}
	
	private static List<String> scanDirectory(File dir) {
		List<String> result = new ArrayList<String>();

		File list[] = dir.listFiles();
		for (File file : list) {
			if (file.isDirectory()) {
				// rekursion!
				result.addAll(scanDirectory(file));
			} else {
				result.add(file.toString());
			}
		}

		return result;
	}
}
