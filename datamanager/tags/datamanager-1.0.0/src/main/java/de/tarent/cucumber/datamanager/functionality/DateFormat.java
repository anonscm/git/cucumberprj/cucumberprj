package de.tarent.cucumber.datamanager.functionality;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Dieser FunktionalitätsInterpreter liefert die
 * aktuelle Zeit als String. Man kann als Parameter
 * einen Formatierungs String übergeben, welcher dann
 * verwendet wird. Siehe dazu {@link SimpleDateFormat}
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class DateFormat implements FunctionalityInterpreter {
	private static final SimpleDateFormat DEFAULT_FMT	=	new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
	
	public String interpret(String functionalityName, String[] args) {
		SimpleDateFormat fmt = DEFAULT_FMT;
		if(args.length > 0){
			fmt = new SimpleDateFormat(args[0]);
		}
		
		return fmt.format(new Date());
	}

}
