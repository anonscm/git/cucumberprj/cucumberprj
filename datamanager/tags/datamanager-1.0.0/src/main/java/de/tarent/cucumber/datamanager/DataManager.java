package de.tarent.cucumber.datamanager;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import de.tarent.cucumber.datamanager.entity.Entity;
import de.tarent.cucumber.datamanager.functionality.DateCalculator;
import de.tarent.cucumber.datamanager.functionality.DateFormat;
import de.tarent.cucumber.datamanager.functionality.FunctionalityInterpreter;

/**
 * Der DataManager verwaltet alle Datensäzte, die in Json-Strukturen
 * im Klassenpfad definiert wurden.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class DataManager {	
	public static final String KEY_ID				=	"_id";
	public static final String KEY_TYPE				=	"_type";
	public static final String KEY_GROUP			=	"_groups";
	public static final String THIS_REFERENCE		=	"this";
	
	public static final String AUTO_GEN_KEY_PREFIX	=	"__auto__";
	public static final String FILE_POSTFIX			=	".json";
	
	private static DataManager _instance;
	
	private static final Pattern referencesPattern = Pattern.compile("\\$\\{[^\\}]*\\}");
	private static final Pattern functionalityPattern = Pattern.compile("#\\{");
	
	private Map<String, Object> managedData = new HashMap<String, Object>();
	private Map<String, FunctionalityInterpreter> functionalityMap = new HashMap<String, FunctionalityInterpreter>();
	
	private boolean isInitialise = false;
	
	DataManager(){
		initDefaultFunctionalities();
	}
	
	public static DataManager getInstance(){
		if(_instance == null){
			_instance = new DataManager();
		}
		
		return _instance;
	}
	
	private void initDefaultFunctionalities(){
		functionalityMap.put("calcDate", new DateCalculator());
		functionalityMap.put("getDate", new DateFormat());
	}
	
	public void init() throws Exception{
		if(isInitialise) return;
		
		List<String> allJsonPath = Utils.getFilesFromClassPathWithPostfix(FILE_POSTFIX);
		List<String> allJson = new ArrayList<String>();
		
		for(String curPath : allJsonPath){
			String fileContent = Utils.readFile(curPath);
			
			try{
				allJson.addAll(Utils.splitStructures(fileContent));
			}catch(Exception e){
				throw new Exception("Error on process json " + curPath, e);
			}
			
			Log.info("Load structure " + curPath);
		}
		for(int i=0; i < allJson.size(); i++){
			if(allJson.get(i).trim().equals("")){
				allJson.remove(i);
				i--; //damit die schleife nicht einen überspringt
			}
		}
		
		parseJsonStrings(allJson);
		isInitialise = true;
	}
	
	/**
	 * Versucht alle Referenzen aufzulösen und parst die JSON-Strukturen in
	 * Objekte, die danach in der ManagedMap aufgenommen werden.
	 * @param jsonStrings
	 * @throws Exception
	 */
	void parseJsonStrings(List<String> jsonStrings) throws Exception{
		List<Integer> ignore = new ArrayList<Integer>();
		List<Integer> ignoreFunctionality = new ArrayList<Integer>();
		
		while(managedData.size() < jsonStrings.size()){

			//marker für das entdecken einer endlosschliefe
			boolean actionPerformed = false;
			for(int i=0; i < jsonStrings.size(); i++){
				//ist json-string bereits geparst?
				if(ignore.contains(i)) continue;
				
				String curJson = jsonStrings.get(i);
				try{
					//ist eine Referenz angegeben
					if(curJson.contains("${")){
						//...löse diese Referenz auf (fals möglich)
						String resolved = resolveReferences(curJson);
						
						//string wurde verändert
						if(!resolved.equals(curJson)){
							jsonStrings.set(i, resolved);
							actionPerformed = true;
						}
					}else if(curJson.contains("#{") && !ignoreFunctionality.contains(i)){
						//... löse die Funktionalitäten auf
						String resolved = resolveFunctionality(curJson);
						
						//string wurde verändert
						if(!resolved.equals(curJson)){
							jsonStrings.set(i, resolved);
						}
						
						ignoreFunctionality.add(i);
						actionPerformed = true;
					}else{
						//diese json-struktur hat keine platzhalter und ist somit parsbar
						parseAndManageJson(curJson);
						
						ignore.add(i);
						actionPerformed = true;
					}
					
				}catch(Exception e){
					throw new Exception("Error on parse json " + curJson, e);
				}
			}
			
			if(!actionPerformed){
				//referenzen werden nicht aufgelöst!
				for(int i=0; i < jsonStrings.size(); i++){
					//ist json-string bereits geparst?
					if(ignore.contains(i)) continue;
					String curJson = jsonStrings.get(i);
					
					System.err.println("Parse json which have non-resolved references included! " + curJson);
					Matcher m = referencesPattern.matcher(curJson);
					
					//durchlaufe alle gefundenen referenzen
					while(m.find()){
						//ermittle den referenzpfad
						String match = m.group();
						String propertyPath = match;
						propertyPath = propertyPath.replace("${", "").replace("}", "");
						
						curJson = curJson.replace(match, propertyPath);
					}
					parseAndManageJson(curJson);
					ignore.add(i);
					break;
				}
			}
		}
	}
	
	private String resolveReferences(String json) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Matcher m = referencesPattern.matcher(json);
		
		//durchlaufe alle gefundenen referenzen
		while(m.find()){
			//ermittle den referenzpfad
			String match = m.group();
			String propertyPath = match;
			propertyPath = propertyPath.replace("${", "").replace("}", "");
			
			try{
				//versuche referenzpfad aufzulösen...
				Object value = null;
				if(propertyPath.startsWith(THIS_REFERENCE + ".")){
					//eine referenz auf sich selbst
					Object parsed = parseJson(json);
					propertyPath = propertyPath.replaceAll("^" + THIS_REFERENCE + "\\.", "");
					
					value = PropertyUtils.getProperty(parsed, propertyPath);
					
					if(value instanceof JsonPrimitive){
						value = ((JsonPrimitive)value).getAsString();
					}
				}else{
					value = get(propertyPath);
				}

				if(value != null){
					//und ersetze den platzhalter mit den referenzierten wert
					if(value instanceof Collection){
						value = Arrays.toString(((Collection)value).toArray());
					}
					
					json = json.replace(match, String.valueOf(value));
				}
			}catch(NestedNullException e){}
		}
		
		return json;
	}
	
	private String resolveFunctionality(String json){
		List<String> toDo = findFunctionalityStrings(json);
		Map<String, String> resolved = new HashMap<String, String>();
		
		for(String match : toDo){
			String functionality = new String(match);
			functionality = functionality.replaceAll("^#\\{", "").replaceAll("\\}$", "");
			
			//verschachtelte werte ersetzen
			if(functionality.contains("#{")){
				for(String pattern : resolved.keySet()){
					functionality = functionality.replace(pattern, resolved.get(pattern));
				}
			}
			
			/* Beispiele:
			 * add(0, 1)
			 * print("test")
			 * now()
			 */
			try{
				String functionalityName = getFunctionalityName(functionality);
				String[] args = getFunctionalityArguments(functionality);
				
				if(functionalityMap.containsKey(functionalityName)){
					String result = functionalityMap.get(functionalityName)
										.interpret(functionalityName, args);
					
					resolved.put(match, result);
				}else{
					Log.error("Could not interpret functionality '" + match + "' because no interpreter will be founded. " +
							  "This string will be not replaced!");
				}
			}catch(Exception e){
				Log.error("Could not interpret functionality '" + match + "'. This string will be not replaced!", e);
			}
		}
		
		//die funde müssen chronologisch durchlaufen werden,
		//da geschachtelte functionalitäten im gesamten ersetzt werden
		//müssen!
		for(int i=toDo.size() - 1; i >= 0; i--){
			String match = toDo.get(i);
			
			if(resolved.containsKey(match)){
				//interpretierten wert mit dem platzhalter ersetzen
				json = json.replace(match, resolved.get(match));
			}
		}
		
		return json;
	}
	
	private String getFunctionalityName(String functionalityString){
		return functionalityString.split("\\(")[0];
	}
	
	public static String[] getFunctionalityArguments(String functionalityString){
		boolean parameterStarted = false;
		boolean ignoreNext = false;
		
		List<String> arguments = new ArrayList<String>();
		StringBuilder curArgument = new StringBuilder();
		Character usedBegin = null;
		for(char c : functionalityString.toCharArray()){
			if(ignoreNext){
				curArgument.append(c);
				ignoreNext = false;
				continue;
			}
			if(c == '('){
				parameterStarted = true;
				continue;
			}
			if(!parameterStarted){
				continue;
			}
			if(c == ')'){
				if(curArgument.length() > 0){
					arguments.add(curArgument.toString());
				}
				break;
			}
			if(c == '\\'){
				ignoreNext = true;
				continue;
			}
			if(	(c == '\'' || c == '"') && usedBegin == null){
				usedBegin = c;
				continue;
			}
			if(usedBegin == null){
				continue;
			}
			
			if(c == usedBegin){
				arguments.add(curArgument.toString());
				curArgument = new StringBuilder();
				
				usedBegin = null;
				continue;
			}
			
			curArgument.append(c);
		}
		
		String[] result = new String[arguments.size()];
		for(int i=0; i < result.length; i++){
			result[i] = arguments.get(i).trim();
		}
		return result;
	}

	private List<String> findFunctionalityStrings(String json){
		List<String> result = new ArrayList<String>();
		List<Integer> position = new ArrayList<Integer>();
		List<Integer> endPositions = new ArrayList<Integer>();
		
		Matcher m = functionalityPattern.matcher(json);
		
		while(m.find()){
			position.add(m.start());
		}
		for(int i = position.size() - 1; i >=0; i--){
			int curStartPosition = position.get(i);
			for(int j=curStartPosition; j < json.length(); j++){
				char c = json.charAt(j);
				
				//ende gefunden und noch nicht für ein anderen anfang benutzt
				if(c == '}' && !endPositions.contains(j)){
					result.add(json.substring(curStartPosition, j + 1));
					endPositions.add(j);
					break;
				}
			}
		}
		
		return result;
	}
	
	private Object parseJson(String json){
		JsonObject jsonObject = (JsonObject)new JsonParser().parse(json);
		Object parsed = null;
		try{
			//ermittle den Typ des Datensatzes
			String type = jsonObject.get(KEY_TYPE).getAsString();
			parsed = new Gson().fromJson(json, Class.forName(type));
		}catch(Exception e){
			//sollte es kein typ geben, wir der Standart-Typ "Map" verwendet
			parsed = Utils.jsonToMap(jsonObject);
		}
		
		return parsed;
	}
	
	private void parseAndManageJson(String json) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Object parsed = parseJson(json);
		
		//ermittle den key des Datensatzes
		Object key = PropertyUtils.getProperty(parsed, KEY_ID);
		boolean alwaysContains = managedData.containsKey(String.valueOf(key));
		
		if(key == null || alwaysContains){
			if(alwaysContains){
				Log.warn("Structur always managed. It will create a unique key and contains. Structure: " + json);
			}
			
			//ist kein key vorhanden, wird einer erstellt...
			key = generateUniqueKey();
		}
		
		//registriere den Datensatz
		managedData.put(key.toString(), parsed);
	}
	
	/**
	 * Erstellt ein einzigartigen Schlüssel, der für
	 * die Datensatzspeicherung verwendet werden kann.
	 * @return
	 */
	private String generateUniqueKey(){
		String generatedKey;
		int i=0;
		
		do{
			i++;
			generatedKey = AUTO_GEN_KEY_PREFIX + i;
		}while(managedData.containsKey(generatedKey));
		
		return generatedKey;
	}
	
	/**
	 * Liefert den Datensatz mit der angegeben id.
	 * Ist kein Datensatz vorhanden, wird <i>null</i> geliefert.
	 * Es ist außerdem möglich property-pfade anzugeben.
	 * @param key
	 * @return
	 */
	public Object get(String propertyPath){
		String[] splitted = propertyPath.split("\\.");
		String mapKey = splitted[0];
		
		StringBuilder propertyKey = new StringBuilder("");
		for(int i=1; i < splitted.length; i++){
			propertyKey.append(splitted[i]);
			if(i + 1 < splitted.length){
				propertyKey.append('.');
			}
		}
		
		Object rootObject = managedData.get(mapKey);
		if(propertyKey.length() == 0){
			return rootObject;
		}else{
			try{
				return PropertyUtils.getProperty(rootObject, propertyKey.toString());
			}catch(Exception e){
				return null;
			}
		}
	}
	
	public boolean contains(String key){
		return get(key) != null;
	}
	
	/**
	 * Liefert alle gemanageten Datensätze einer bestimten Klasse.
	 * @param <T>
	 * @param type
	 * @return
	 */
	public <T> List<T> getSpecificValues(Class<T> type){
		List<T> result = new ArrayList<T>();
		
		for(Object curValue : managedData.values()){
			if(type.isInstance(curValue)){
				result.add((T)curValue);
			}
		}
		
		return result;
	}
	
	/**
	 * Liefert alle gemanageten Datensätze, die in der groupId den
	 * gewünschten Wert stehen haben.
	 * 
	 * @param groupId
	 * @return
	 */
	public List<Entity> getGroupedEntities(String groupId){
		List<Entity> result = new ArrayList<Entity>();
		
		for(Object value : managedData.values()){
			if(value instanceof Entity){
				Entity e = (Entity)value;
				if((e.get_groups() == null || e.get_groups().size() == 0) && groupId == null){
					result.add(e);
				}else if(e.get_groups() != null && e.get_groups().contains(groupId)){
					result.add(e);
				}
			}
		}
		
		return result;
	}
	
	public <T extends Entity> List<T> getGroupedEntities(String groupId, Class<T> clazz){
		List<T> result = new ArrayList<T>();
		
		List<Entity> entities = getGroupedEntities(groupId);
		for(Entity entity : entities){
			if(clazz.isInstance(entity)){
				result.add((T)entity);
			}
		}
		
		return result;
	}
	
	/**
	 * Registriert einen Interpreter für erweiterte Funktionalitäten.
	 * @param functionalityName Der Name der Funktionalität, der durch den Intepreter übersetzt werden soll.
	 * @param interpreter Interpreter, der die Übersetzungsarbeit übernimmt.
	 */
	public void registerFunctionalityInterpreter(String functionalityName,
			FunctionalityInterpreter interpreter){
		
		functionalityMap.put(functionalityName, interpreter);
	}
}
