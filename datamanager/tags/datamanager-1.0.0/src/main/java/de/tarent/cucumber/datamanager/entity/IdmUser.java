package de.tarent.cucumber.datamanager.entity;

import java.util.List;
import java.util.Map;

public class IdmUser extends Entity{

	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private List<String> roles;
	private List<String> groups;
	private String uuid;
	private Map<String, Map<String, String>> values;
	
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Map<String, Map<String, String>> getValues() {
		return values;
	}
	public void setValues(Map<String, Map<String, String>> values) {
		this.values = values;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	public List<String> getGroups() {
		return groups;
	}
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
