package de.tarent.cucumber.datamanager.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Entity {
	private String _id;
	private String _type;
	private List<String> _groups = new ArrayList<String>();
	private Map<String, String> _data;

	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String get_type() {
		return _type;
	}
	public void set_type(String _type) {
		this._type = _type;
	}
	public List<String> get_groups() {
		return _groups;
	}
	public void set_groups(List<String> _groups) {
		this._groups = _groups;
	}
	public Map<String, String> get_data() {
		return _data;
	}
	public void set_data(Map<String, String> _data) {
		this._data = _data;
	}
}