package de.tarent.cucumber.datamanager.functionality;

public class FunctionalityException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public static final String MESSAGE_INVALID_PARAMETER	=	"Invalid parameters!";

	public FunctionalityException() {
		super();
	}

	public FunctionalityException(String message, Throwable cause) {
		super(message, cause);
	}

	public FunctionalityException(String message) {
		super(message);
	}

	public FunctionalityException(Throwable cause) {
		super(cause);
	}
	
}
