package de.tarent.cucumber.datamanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import de.tarent.cucumber.datamanager.entity.DataSet;
import de.tarent.cucumber.datamanager.entity.Field;
import de.tarent.cucumber.datamanager.entity.IdmUser;
import de.tarent.cucumber.datamanager.functionality.FunctionalityException;
import de.tarent.cucumber.datamanager.functionality.FunctionalityInterpreter;

public class TestDataManager {

	@Test
	public void testNormalJsonParsing(){
		DataManager manager = new DataManager();
		
		//type = null -> Map should be created!
		final String mapJson = "{'" + DataManager.KEY_ID + "':'mapJson', 'vorname':'Sven', 'nachname':'Schumann'}";
		//IdmUser
		final String userJson = "{" +
									"'" + DataManager.KEY_ID + "':'idm'," +
									"'" + DataManager.KEY_TYPE + "':'de.tarent.cucumber.datamanager.entity.IdmUser'," +
									"'" + DataManager.KEY_GROUP + "':['user']," +
									"'firstname':'Sven'," +
									"'lastname':'Schumann'," +
									"'email':'s.schumann@tarent.de'," +
									"'password':'Vibesa10'," +
									"'roles':['bewerber', 'stipendiat']," +
									"'values':{" +
									"	'personenData':{" +
									"		'telefon':'1234567', " +
									"		'email':'${" + DataManager.THIS_REFERENCE + ".email}'" +
									"	}" +
									"}" +
								"}";
		//datarecord
		final String dataRecordJson = 	"{" +
											"'" + DataManager.KEY_ID + "':'REGISTRIERUNG', " +
											"'" + DataManager.KEY_TYPE + "':'de.tarent.cucumber.datamanager.entity.DataSet', " +
											"'values': [" +
												"{" +
													"'id':'vorname', " +
													"'label':'Vorname:', " +
													"'xpath':'//*[@id=\"vorname\"]', " +
													"'type':'input', " +
													"'value':'Sven'" +
												"}" +
											"]" +
										"}";
		
		try {
			manager.parseJsonStrings(TestUtils.toList(mapJson, userJson, dataRecordJson));
			
			Object resolvedMapJson = manager.get("mapJson");
			assertNotNull(resolvedMapJson);
			assertTrue(resolvedMapJson instanceof Map<?, ?>);
			
			assertEquals("Sven", ((Map)resolvedMapJson).get("vorname"));
			assertEquals("Schumann", ((Map)resolvedMapJson).get("nachname"));
			assertEquals(1, manager.getSpecificValues(Map.class).size());
			
			//idmuser
			Object resolvedUser = manager.get("idm");
			assertNotNull(resolvedUser);
			assertTrue(resolvedUser instanceof IdmUser);
			assertEquals(1, manager.getSpecificValues(IdmUser.class).size());
			assertEquals(1, manager.getGroupedEntities("user").size());
			assertEquals("s.schumann@tarent.de", ((IdmUser)resolvedUser).getEmail());
			assertEquals(2, ((IdmUser)resolvedUser).getRoles().size());
			assertNull(((IdmUser)resolvedUser).getGroups());
			assertNotNull(((IdmUser)resolvedUser).getValues().get("personenData"));
			assertEquals(((IdmUser)resolvedUser).getEmail(), 
					((IdmUser)resolvedUser).getValues().get("personenData").get("email"));
			
			//datarecord
			Object resolvedDataRecord = manager.get("REGISTRIERUNG");
			assertNotNull(resolvedUser);
			assertTrue(resolvedDataRecord instanceof DataSet);
			assertEquals(1, ((DataSet)resolvedDataRecord).getValues().size());
			
			Field resolvedField = ((DataSet)resolvedDataRecord).getValues().get(0);
			assertEquals("vorname", resolvedField.getId());
			assertEquals("Vorname:", resolvedField.getLabel());
			assertEquals("//*[@id=\"vorname\"]", resolvedField.getXpath());
			assertEquals("input", resolvedField.getType());
			assertEquals("Sven", resolvedField.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testReferenzResolveJsonParsing(){
		DataManager manager = new DataManager();
		
		final String jsonParent = "{'" + DataManager.KEY_ID + "':'parent', 'name':'Schumann', 'vorname':'Sven'}";
		final String jsonChild = "{'" + DataManager.KEY_ID + "':'child', 'vorname':'Fynn', 'vater':'${parent.vorname} ${parent.name}'}";
		
		try{
			manager.parseJsonStrings(TestUtils.toList(jsonChild, jsonParent));
			
			assertNotNull(manager.get("parent"));
			assertNotNull(manager.get("child"));
			assertEquals("Sven Schumann", ((Map)manager.get("child")).get("vater"));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	//TODO
	@Ignore("Dies ist ein Test für gegenseitige Abhängigkeiten. Dies sorgt noch dafür, dass eine Endlosschleife entsteht!")
	@Test
	public void testBidirectionalReferenzResolveJsonParsing(){
		DataManager manager = new DataManager();
		
		final String jsonParent = "{'" + DataManager.KEY_ID + "':'parent', 'name':'Schumann', 'vorname':'Sven', 'kind':'${child.vorname}'}";
		final String jsonChild = "{'" + DataManager.KEY_ID + "':'child', 'vorname':'Fynn', 'vater':'${parent.vorname} ${parent.name}'}";
		
		try{
			manager.parseJsonStrings(TestUtils.toList(jsonChild, jsonParent));
			
			assertNotNull(manager.get("parent"));
			assertNotNull(manager.get("child"));
			assertEquals("Fynn", ((Map)manager.get("parent")).get("kind"));
			assertEquals("Sven Schumann", ((Map)manager.get("child")).get("vater"));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testMultipleJsons(){
		DataManager manager = new DataManager();
		
		final String jsonParent = "{'" + DataManager.KEY_ID + "':'parent', 'name':'Schumann', 'vorname':'Sven'}";
		final String jsonChild = "{'" + DataManager.KEY_ID + "':'child', 'vorname':'Fynn', 'vater':'${parent.vorname} ${parent.name}'}";
		
		try{
			manager.parseJsonStrings(Utils.splitStructures((jsonParent + " " + jsonChild)));
			
			assertNotNull(manager.get("parent"));
			assertNotNull(manager.get("child"));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testInitialising(){
		DataManager manager = new DataManager();
	
		try{
			manager.init();

			boolean foundAdmin = false;
			boolean foundUser1 = false;
			boolean foundUser2 = false;
			
			for(IdmUser user : manager.getSpecificValues(IdmUser.class)){
				if(user.get_id() != null){
					if(user.get_id().equals("__idmAdmin")){
						foundAdmin = true;
					}else if(user.get_id().equals("__user1")){
						foundUser1 = true;
					}else if(user.get_id().equals("__user2")){
						foundUser2 = true;
					}
				}
			}
			
			assertTrue(foundAdmin);
			assertTrue(foundUser1);
			assertTrue(foundUser2);
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testFunctionalityInterpreter(){
		DataManager manager = new DataManager();
		manager.registerFunctionalityInterpreter("out", new FunctionalityInterpreter() {
			public String interpret(String functionalityName, String[] args)
					throws FunctionalityException {
				return args[0];
			}
		});
		manager.registerFunctionalityInterpreter("func", new FunctionalityInterpreter() {
			public String interpret(String functionalityName, String[] args)
					throws FunctionalityException {
				return args[0] + " " + args[1];
			}
		});
		
		final String json = "{'" + DataManager.KEY_ID + "':'test', 'word':'hallo', 'value':'#{func(\"${this.word}\", \"#{out(\"welt\")}\")}'}";
		String expected = "hallo welt";
		
		try{
			manager.parseJsonStrings(TestUtils.toList(json));
			
			Object resolved = manager.get("test");
			assertNotNull(resolved);
			assertEquals(expected, ((Map)resolved).get("value"));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
}
