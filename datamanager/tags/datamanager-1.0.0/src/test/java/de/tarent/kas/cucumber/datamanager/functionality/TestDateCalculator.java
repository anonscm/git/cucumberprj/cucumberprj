package de.tarent.kas.cucumber.datamanager.functionality;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import de.tarent.cucumber.datamanager.functionality.DateCalculator;
import de.tarent.cucumber.datamanager.functionality.FunctionalityInterpreter;

public class TestDateCalculator {

	@Test
	public void testInterpreter(){
		FunctionalityInterpreter toTest = new DateCalculator();
		
		try{
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy");
			Calendar c = new GregorianCalendar();
			c.add(Calendar.YEAR, 1);
			String expectedOutput = fmt.format(c.getTime());
			
			assertEquals(expectedOutput, toTest.interpret("wayne", new String[]{"+1y", "yyyy"}));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
}
