# language: de
Funktionalität: Eine Demonstration wie man DataTables verwenden kann


Szenario: Überprüfung ob die Datensatz Anzahl und der Inhalt stimmt

Angenommen ich habe folgende Datentabelle:
	|	Eins	|
	|	Zwei	|
Dann existieren darin "2" Zeilen
Und eine Zeile davon besitzt den Inhalt "Eins"
Und eine Zeile davon besitzt den Inhalt "Zwei"


Szenario: Überprüfung des Datensatz Inhaltes

Angenommen ich konvertiere folgende Datentabelle:
	|	Vorname	|	Nachname	|	Beruf				|
	|	Sven	|	Schumann	|	Softwareentwickler	|
	|	Ulf		|	Knödel		|	Koch				|
	|	Berta	|	Buschwald	|	Försterin			|
Wenn "3" Personen eingelesen wurden
Dann ist "Sven" von Beruf "Softwareentwickler"
Dann ist "Ulf" von Beruf "Koch"
Dann ist "Berta" von Beruf "Försterin"
