package de.tarent.cucumber.example.formatter;

import gherkin.formatter.PrettyFormatter;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.Step;

/**
 * This class is a workaround for a missing hook!
 * Because the hook which is called after each step
 * doesn't provide information about the current 
 * feature or current scenario and so on. This class
 * represents a formatter that we refer to the 
 * cucumber-cli. 
 *
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class RuntimeInfoCatcher extends PrettyFormatter {
	public static final String COLORED_OUTPUT_SYSTEM_PROPERTY	=	"tarent.cucumber.colored";
	
	private static RuntimeInfoCatcher instance;
	
	private Feature currentFeature;
	private Scenario currentScenario;
	private Step currentStep;
	
	private int currentScenarioCount = 0;
	private int currentStepCount = 0;
	
	public RuntimeInfoCatcher(Appendable appendable, boolean monochrome, boolean executing){
		super(appendable, 
				! Boolean.valueOf(System.getProperty(COLORED_OUTPUT_SYSTEM_PROPERTY, Boolean.TRUE.toString())), 
				executing);

		instance = this;
	}
	
	public RuntimeInfoCatcher(Appendable appendable){
		super(appendable,
				! Boolean.valueOf(System.getProperty(COLORED_OUTPUT_SYSTEM_PROPERTY, Boolean.TRUE.toString())),
				true);
		
		instance = this;
	}
	
	public static RuntimeInfoCatcher getInstance(){
		return instance;
	}
	
	@Override
	public void feature(Feature feature) {
		super.feature(feature);
		
		if(!feature.equals(currentFeature)){
			currentScenarioCount = 0;
			currentStepCount = 0;
		}
		
		this.currentFeature = feature;
	}

	@Override
	public void scenario(Scenario scenario) {
		super.scenario(scenario);
		
		if(!scenario.equals(currentScenario)){
			currentScenarioCount++;
			currentStepCount = 0;
		}
		
		this.currentScenario = scenario;
	}

	@Override
	public void step(Step step) {
		super.step(step);
		
		if(!step.equals(currentStep)){
			currentStepCount++;
		}
		
		this.currentStep = step;
	}

	public Feature getCurrentFeature() {
		return currentFeature;
	}
	public Scenario getCurrentScenario() {
		return currentScenario;
	}
	public Step getCurrentStep() {
		return currentStep;
	}
	public int getCurrentScenarioCount() {
		return currentScenarioCount;
	}
	public int getCurrentStepCount() {
		return currentStepCount;
	}
}
