package de.tarent.cucumber.example.env.event;

import cucumber.runtime.ScenarioResult;

public interface HandlerAfter {

	/**
	 * This will be called after each scenario.
	 * 
	 * @param result
	 */
	public void handleAfterScenario(ScenarioResult result);
}
