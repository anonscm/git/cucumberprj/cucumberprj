package de.tarent.cucumber.example.env.event;

public interface HandlerStop {

	/**
	 * This will be called after the program will be exit.
	 */
	public void handleStop();
}
