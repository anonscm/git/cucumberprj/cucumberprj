package de.tarent.cucumber.example.stepdefinitions;

import static org.junit.Assert.*;

import java.util.List;

import cucumber.annotation.de.Angenommen;
import cucumber.annotation.de.Dann;
import cucumber.annotation.de.Und;
import cucumber.annotation.de.Wenn;
import cucumber.table.DataTable;
import de.tarent.cucumber.example.entity.Person;

public class StepDefinitions {
	
	/********************************
	 * YOU CAN PUT YOUR STEPDEFINITIONS HERE
	 ********************************/

	private DataTable currentDataTable;
	private	List<Person> currentPersonen;	
	
	@Angenommen("^ich habe folgende Datentabelle:$")
	public void ich_habe_folgende_Datentabelle(DataTable dt){
		currentDataTable = dt;
	}
	
	@Dann("^existieren darin \"([^\"]*)\" Zeilen$")
	public void existieren_darin_N_Zeilen(Integer n){
		assertTrue(currentDataTable.raw().size() == n);
	}
	
	@Und("^eine Zeile davon besitzt den Inhalt \"([^\"]*)\"$")
	public void eine_Zeile_davon_besitzt_den_Inhalt(String content){
		boolean found = false;
		
		for(List<String> level1 : currentDataTable.raw()){
			for(String value : level1){
				if(content.equals(value)){
					found = true;
					break;
				}
			}
			if(found) break;
		}
		
		assertTrue(found);
	}

	@Angenommen("^ich konvertiere folgende Datentabelle:$")
	public void ich_konvertiere_folgende_Datentabelle(List<Person> personen){
		currentPersonen = personen;
	}
	
	@Wenn("^\"([^\"]*)\" Personen eingelesen wurden$")
	public void n_Personen_eingelesen_wurden(Integer n){
		assertTrue(currentPersonen.size() == n);
	}
	
	@Dann("ist \"([^\"]*)\" von Beruf \"([^\"]*)\"")
	public void ist_von_Beruf(String vorname, String beruf){
		for(Person curPerson : currentPersonen){
			if(curPerson.getVorname().equals(vorname)){
				assertEquals(curPerson.getBeruf(), beruf);
				break;
			}
		}
	}
}
