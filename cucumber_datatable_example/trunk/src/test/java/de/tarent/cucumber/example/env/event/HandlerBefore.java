package de.tarent.cucumber.example.env.event;

public interface HandlerBefore {

	/**
	 * This will be called before each scenario.
	 */
	public void handleBeforeScenario();
}
