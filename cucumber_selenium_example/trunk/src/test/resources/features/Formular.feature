# language: de
Funktionalität: Befüllung und Auswertung von größeren Masken via DataManager


Szenario: Automatische befüllung der Testmaske

Angenommen die Formular-Testseite ist geöffnet
Wenn ich die Maske mit den Daten "FORMULAR_EINGABE" befülle
Dann sind die Felder mit den Daten "FORMULAR_AUSGABE" vorbefüllt
