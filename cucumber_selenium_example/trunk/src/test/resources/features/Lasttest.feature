# language: de
Funktionalität: Eine kleine Demonstration, wie ein simpler Lasttest aussehen könnte


Szenario: Aufrufen von x Seiten darf nicht länger als y Sekunden dauern

Angenommen ich fange an die Zeit zu stoppen
Wenn ich alle Seiten von "LASTTEST_URL_LIST" aufrufe
Und anschließend die Zeit stoppe
Dann sollte es nicht mehr wie "8" Sekunden gedauert haben


Szenario: Aufrufen von x Seiten darn nicht länger als y Sekunden dauern (mittels DataTables)

Angenommen ich fange an die Zeit zu stoppen
Wenn ich alle folgenden Seiten aufrufe
	| http://www.tarent.de     |
	| http://www.wikipedia.de  |
	| http://www.google.de     |
	| http://www.tarent.com    |
Und anschließend die Zeit stoppe
Dann sollte es nicht mehr wie "8" Sekunden gedauert haben