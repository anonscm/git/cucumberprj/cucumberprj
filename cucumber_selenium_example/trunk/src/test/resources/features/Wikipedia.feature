# language: de
Funktionalität: Einfache Benutzung von Selenium am Beispiel "Wikipedia"


Szenario: Öffnen einer Wikipedia-Seite

Wenn ich die Seite "http://www.wikipedia.de" öffne
Und ich in das Eingabefeld mit der ID "txtSearch" den Wert "C++" eingebe
Und auf den Button mit der ID "cmdSearch" klicke
Dann erhalte ich einen Abschnitt mit dem Titel "Der Name „C++“"


Szenario: Fehler auslösen -> dies führt dazu, dass ein Screenshot erstellt wird

Angenommen ich bin auf der Seite "http://de.wikipedia.org/wiki/C%2B%2B"
Dann löse ich einen Fehler aus
