package de.tarent.cucumber.example.env.event;

public interface HandlerStart {

	/**
	 * This will be called before cucumber starts.
	 */
	public void handleStart();
}
