package de.tarent.cucumber.example.env;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import cucumber.annotation.After;
import cucumber.annotation.Before;
import cucumber.runtime.ScenarioResult;
import de.tarent.cucumber.datamanager.DataManager;
import de.tarent.cucumber.example.env.event.HandlerAfter;
import de.tarent.cucumber.example.env.event.HandlerBefore;
import de.tarent.cucumber.example.env.event.HandlerStart;
import de.tarent.cucumber.example.env.event.HandlerStop;

/**
 * This class contains all important (cucumber-)hooks.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class StartAndShutdownHook {
	
	private static List<HandlerAfter> afterHandler = new ArrayList<HandlerAfter>();
	private static List<HandlerBefore> beforeHandler = new ArrayList<HandlerBefore>();
	private static List<HandlerStart> startHandler = new ArrayList<HandlerStart>();
	private static List<HandlerStop> stopHandler = new ArrayList<HandlerStop>();
	
	private static boolean initialised = false;
	/**
	 * This hook will be called before cucumber starts
	 * to process features.
	 */
	@PostConstruct
	public void postConstruct() throws Exception{
		if(!initialised) initialised = true;
		else return;
		
		//initialise shutdown-hook
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() {
		    	preDestroy();
		    }
		});
		
		//forward event
		for(HandlerStart handler : startHandler){
			handler.handleStart();
		}

		//initialize datamanager
		DataManager.getInstance().init();
	}
	
	/**
	 * This hook will be called before each scenario.
	 */
	@Before
	@org.junit.Before
	public void beforeScenario(){
		//forward event
		for(HandlerBefore handler : beforeHandler){
			handler.handleBeforeScenario();
		}
	}
	
	/**
	 * This hook will be called after each step.
	 */
	@After
	@org.junit.After
	public void afterScenario(ScenarioResult result){
		//forward event
		for(HandlerAfter handler : afterHandler){
			handler.handleAfterScenario(result);
		}
	}
	
	/**
	 * This hook will be called before the program will exit.
	 */
	private static void preDestroy(){
		//forward event
		for(HandlerStop handler : stopHandler){
			handler.handleStop();
		}
	}
	
	//event handler functions
	public static void addOnStartHandler(HandlerStart handler){
		startHandler.add(handler);
	}
	
	public static void addOnStopHandler(HandlerStop handler){
		stopHandler.add(handler);
	}
	
	public static void addOnAfterHandler(HandlerAfter handler){
		afterHandler.add(handler);
	}
	
	public static void addOnBeforeHandler(HandlerBefore handler){
		beforeHandler.add(handler);
	}
}
