package de.tarent.cucumber.example.stepdefinitions;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.annotation.de.Angenommen;
import cucumber.annotation.de.Dann;
import cucumber.annotation.de.Und;
import de.tarent.cucumber.datamanager.entity.Field;
import de.tarent.cucumber.example.env.AdvancedSelenium;

public class FormularSteps {

	@Autowired
	private WebDriver browser;
	
	@Autowired
	private AdvancedSelenium advancedBrowser;

	@Angenommen("^die Formular-Testseite ist geöffnet$")
	public void die_Formular_Testseite_ist_geoeffnet(){
		browser.get(getClass().getResource("/htmltest/Formular.html").toString());
	}
	
	@Und("^ich die Maske mit den Daten \"([^\"]*)\" befülle$")
	public void ich_die_Maske_mit_befuelle(String dataSet){
		advancedBrowser.fillPageFields(dataSet);
	}
	
	@Dann("^(sind |)?die Felder (sind |)?mit den Daten \"([^\"]*)\" vorbefüllt$")
	public void die_Felder_mit_den_Daten_vorbefuellt(String t0, String t1, String dataSet){
		List<Field> errors = advancedBrowser.checkPageFields(dataSet);
		
		for(Field error : errors){
			System.err.println("Das Feld '" + error.getLabel() + "' ist nicht mit dem Wert " +
					"'" + error.getValue() + "', sondern mit '" + error.get_value() + "' vorbefüllt.");
		}
		
		assertEquals("Unerwartete Ausgaben!",
				0, errors.size());
	}
}
