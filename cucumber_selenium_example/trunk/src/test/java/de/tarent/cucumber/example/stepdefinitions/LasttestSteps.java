package de.tarent.cucumber.example.stepdefinitions;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.annotation.de.Angenommen;
import cucumber.annotation.de.Dann;
import cucumber.annotation.de.Und;
import cucumber.annotation.de.Wenn;
import de.tarent.cucumber.datamanager.DataManager;
import de.tarent.cucumber.example.data.entity.LasttestUrlContainer;

public class LasttestSteps {

	@Autowired
	private WebDriver browser;
	
	private long startTime = 0L;
	private long stopTime = 0L;
	
	@Angenommen("ich fange an die Zeit zu stoppen")
	public void ich_fange_an_die_Zeit_zu_stoppen(){
		startTime = System.currentTimeMillis();
	}
	
	@Wenn("ich alle Seiten von \"([^\"]*)\" aufrufe")
	public void ich_alle_Seiten_von_aufrufe(String ds){
		LasttestUrlContainer container = (LasttestUrlContainer)DataManager.getInstance().get(ds);
		
		for(String url : container.getUrlList()){
			browser.get(url);
		}
	}
	
	@Wenn("ich alle folgenden Seiten aufrufe")
	public void ich_alle_folgenden_Seiten_aufrufe(List<List<String>> urls){
		List<String> urlList = new ArrayList<String>();
		
		for(List<String> level1 : urls){
			for(String url : level1){
				urlList.add(url);
			}
		}
		
		for(String url : urlList){
			browser.get(url);
		}
	}
	
	@Und("anschließend die Zeit stoppe")
	public void anschliessend_die_Zeit_stoppe(){
		stopTime = System.currentTimeMillis();
	}
	
	@Dann("sollte es nicht mehr wie \"([^\"]*)\" Sekunden gedauert haben")
	public void sollte_es_nicht_mehr_wie_Sekunden_gedauert_haben(Integer sek){
		long duration = stopTime - startTime;
		long max = sek * 1000;
		
		assertTrue("Die Prozedur hat " + max / 100 + " sek. gedauert!", 
				duration <= max);
	}
}
