package de.tarent.cucumber.example.data.entity;

import java.util.List;

import de.tarent.cucumber.datamanager.entity.Entity;

public class LasttestUrlContainer extends Entity{
	
	private List<String> urlList;

	public List<String> getUrlList() {
		return urlList;
	}
	public void setUrlList(List<String> urlList) {
		this.urlList = urlList;
	}
}
