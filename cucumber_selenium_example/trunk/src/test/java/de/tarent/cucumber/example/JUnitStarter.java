package de.tarent.cucumber.example;

import org.junit.runner.RunWith;

import cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(
		//this code will only look into "features/" folder for features
		features={"classpath:features/"},
			
		//DO NOT REMOVE THIS FORMATTER!
		format={"de.tarent.cucumber.example.formatter.RuntimeInfoCatcher"})
public class JUnitStarter {

	/*******************************************************
	 * DO NOT ADD SOME METHODS! THIS CLASS MUST BE EMPTY!!! 
	 *******************************************************/
}
