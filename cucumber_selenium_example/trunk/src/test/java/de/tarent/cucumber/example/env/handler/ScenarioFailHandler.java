package de.tarent.cucumber.example.env.handler;

import java.io.ByteArrayInputStream;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.runtime.ScenarioResult;
import de.tarent.cucumber.example.env.StartAndShutdownHook;
import de.tarent.cucumber.example.env.event.HandlerAfter;

public class ScenarioFailHandler implements HandlerAfter{

	@Autowired
	private WebDriver browser;
	
	public ScenarioFailHandler(){
		StartAndShutdownHook.addOnAfterHandler(this);
	}

	@Override
	public void handleAfterScenario(ScenarioResult result) {
		if(result.isFailed()){
			////
			//save a screenshot
			////
			byte[] screenshot = null;
			boolean successfullyTaken = true;
			
			//check webdriver for screenshot-functionality
			if(browser instanceof TakesScreenshot){
				screenshot = ((TakesScreenshot)browser).getScreenshotAs(OutputType.BYTES);
			}
			
			if(screenshot == null || screenshot.length == 0){
				System.err.println("Error on taking screenshot. Apparently this functionality is not available.");
				successfullyTaken = false;
			}
			
			if(successfullyTaken){
				result.embed(new ByteArrayInputStream(screenshot), "image/png");
			}
		}
	}
}
