package de.tarent.cucumber.example.env;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import de.tarent.cucumber.datamanager.DataManager;
import de.tarent.cucumber.datamanager.entity.DataSet;
import de.tarent.cucumber.datamanager.entity.Field;

public class AdvancedSelenium {
	public static final String HTML_TYPE_SPAN			=	"span";
	public static final String HTML_TYPE_SELECT			=	"select";
	public static final String HTML_TYPE_TEXTAREA		=	"textarea";
	public static final String HTML_TYPE_CHECKBOX		=	"checkbox";
	public static final String HTML_TYPE_INPUT			=	"input";
	public static final String HTML_TYPE_RADIO			=	"radio";
	
	@Autowired
	private WebDriver browser;
	
	private DataManager manager;
	
	
	public AdvancedSelenium(WebDriver driver) throws Exception{
		this.browser = driver;
		this.manager = DataManager.getInstance();
		
		//ein mehrfaches aufrufen der init-methode hat keinerlei
		//negative auswirkungen!
		this.manager.init();
	}

	private void checkDataSet(String dataSetName, Class<?> instanceClass){
		if(!manager.contains(dataSetName)){
			throw new RuntimeException("Es wurde kein Datensatz mit den Namen '" + dataSetName + "' gefunden !");
		}
		
		Object o = manager.get(dataSetName);
		if(!instanceClass.isInstance(o)){
			throw new RuntimeException("Der gefundene Datensatz (" + dataSetName + ") wird nicht unterstützt!");
		}
	}
	
	public List<Field> checkPageFields(String dataSetName){
		checkDataSet(dataSetName, DataSet.class);
		DataSet set = (DataSet)manager.get(dataSetName);
		
		List<Field> resultErrors = new ArrayList<Field>();
		for(Field field : set.getValues()){
			if(field == null) continue;	//kann vorkommen, wenn Benutzer das komma hinter dem letzen eintrag nicht weggemacht hat!
			
			if(field.getSkip() != null){
				//mit diesem wert gekennzeichnete felder werden übersprungen!
				printSkipMessage(field);
				continue;
			}
			
			if(field.getType() == null){
				throw new RuntimeException("Ungültiger Typ des Feldes '" + field.getLabel() + " (" + field.getId() + ")'!");
			}
			//check span
			if(field.getType().equalsIgnoreCase(HTML_TYPE_SPAN)){
				if(!checkSpan(field)){
					resultErrors.add(field);
				}
			//check radio,checkbox,input
			}else if(	field.getType().equalsIgnoreCase(HTML_TYPE_CHECKBOX) ||
						field.getType().equalsIgnoreCase(HTML_TYPE_INPUT) ||
						field.getType().equalsIgnoreCase(HTML_TYPE_RADIO)){
				
				if(!checkInput(field)){
					resultErrors.add(field);
				}
			}else if(field.getType().equalsIgnoreCase(HTML_TYPE_TEXTAREA)){
				if(!checkTextarea(field)){
					resultErrors.add(field);
				}
			}else if(field.getType().equalsIgnoreCase(HTML_TYPE_SELECT)){
				if(!checkSelect(field)){
					resultErrors.add(field);
				}
			}else{
				throw new RuntimeException("Ungültiger Typ des Feldes '" + field.getLabel() + " (" + field.getId() + ")'!");
			}
		}
		
		return resultErrors;
	}
	
	private void printSkipMessage(Field field){
		System.out.println("[WARN]Das Datum " + field.getLabel() + "(" + field.getId() + ") wird nicht abgetestet, " +
				"da es als >Skip< markiert wurde. Der genannte Grund: " + field.getSkip());
	}

	public void fillPageFields(String dataSetName){
		checkDataSet(dataSetName, DataSet.class);
		fillPageFields((DataSet)manager.get(dataSetName));
	}

	public List<Field> checkRequiredFields(String dataSetName){
		checkDataSet(dataSetName, DataSet.class);
		
		DataSet set = (DataSet)manager.get(dataSetName);
		List<Field> fields = new ArrayList<Field>();
		for(Field field : set.getValues()){
			if(field == null) continue;	//kann vorkommen, wenn Benutzer das komma hinter dem letzen eintrag nicht weggemacht hat!
			
			if(	field.getType() == null ||
				field.getType().equalsIgnoreCase(HTML_TYPE_CHECKBOX) ||
				field.getType().equalsIgnoreCase(HTML_TYPE_INPUT) ||
				field.getType().equalsIgnoreCase(HTML_TYPE_RADIO) ||
				field.getType().equalsIgnoreCase(HTML_TYPE_SELECT) ||
				field.getType().equalsIgnoreCase(HTML_TYPE_TEXTAREA)){
				
				try{
					browser.findElement(By.xpath("//*[@id='" + field.getId() + "' and @class='error']"));
				}catch(NoSuchElementException e){
					fields.add(field);
				}
			}
		}
		
		return fields;		
	}
	
	private void fillPageFields(DataSet set){
		for(Field field : set.getValues()){
			if(field == null) continue;	//kann vorkommen, wenn Benutzer das komma hinter dem letzen eintrag nicht weggemacht hat!
			
			if(field.getSkip() != null){
				//mit diesem wert gekennzeichnete felder werden übersprungen!
				printSkipMessage(field);
				continue;
			}
			
			if(field.getType().equalsIgnoreCase(HTML_TYPE_CHECKBOX)){
				setCheckboxValue(field);
			}else if(field.getType().equalsIgnoreCase(HTML_TYPE_INPUT)){
				setInputValue(field);
			}else if(field.getType().equalsIgnoreCase(HTML_TYPE_TEXTAREA)){
				setTextareaValue(field);
			}else if(field.getType().equalsIgnoreCase(HTML_TYPE_SELECT)){
				setSelectValue(field);
			}else if(field.getType().equalsIgnoreCase(HTML_TYPE_RADIO)){
				setRadioValue(field);
			}
		}
	}
	
	private void setCheckboxValue(Field field){
		WebElement element = null;
		
		if(field.getId() != null){
			element = browser.findElement(By.xpath("//input[@id='" + field.getId() + "']"));
		}else if(field.getXpath() != null){
			element = browser.findElement(By.xpath(field.getXpath()));
		}else{
			//TODO:Implements Label referencing
			throw new RuntimeException("Kann das angegebene Feld nicht befüllen! " + field);
		}

		if("false".equals(field.getValue())){
			if(element.isSelected()){
				element.click();
			}
		}else{
			//we want to select it (if is always selected then do nothing)
			if(!element.isSelected()){
				element.click();
			}
		}
	}
	
	private void setInputValue(Field field){
		WebElement element = null;
		
		if(field.getId() != null){
			element = browser.findElement(By.xpath("//input[@id='" + field.getId() + "']"));
		}else if(field.getXpath() != null){
			element = browser.findElement(By.xpath(field.getXpath()));
		}else{
			//TODO:Implements Label referencing
			throw new RuntimeException("Kann das angegebene Feld nicht befüllen! " + field);
		}
		
		element.clear();
		element.sendKeys(field.getValue());
	}
	
	private void setTextareaValue(Field field){
		WebElement element = null;
		
		if(field.getId() != null){
			element = browser.findElement(By.xpath("//textarea[@id='" + field.getId() + "']"));
		}else if(field.getXpath() != null){
			element = browser.findElement(By.xpath(field.getXpath()));
		}else{
			//TODO:Implements Label referencing
			throw new RuntimeException("Kann das angegebene Feld nicht befüllen! " + field);
		}
		
		element.clear();
		element.sendKeys(field.getValue());
	}
	
	private void setSelectValue(Field field){
		if(	field.getValue() == null ||
			field.getValue().equals("")){
		
			//do not select anything
			return;
		}
		
		String xpath = null;
		
		if(field.getId() != null){
			xpath = "//select[@id='" + field.getId() + "']";
		}else if(field.getXpath() != null){
			xpath = field.getXpath();
		}else{
			//TODO:Implements Label referencing
			throw new RuntimeException("Kann das angegebene Feld nicht befüllen! " + field);
		}
		
		List<WebElement> options = browser.findElement(By.xpath(xpath))
			.findElements(By.tagName("option"));
		
		for(WebElement option : options){
			String valueAttribute = option.getAttribute("value");
			
			if(	option.getText().equals(field.getValue()) ||
				(valueAttribute != null && valueAttribute.equals(field.getValue()))){
				
				option.click();
				break; //we found it!
			}
		}
	}
	
	private void setRadioValue(Field field){
		WebElement element = null;
		
		if(field.getId() != null){
			element = browser.findElement(By.xpath("//input[@id='" + field.getId() + "']"));
		}else if(field.getXpath() != null){
			element = browser.findElement(By.xpath(field.getXpath()));
		}else{
			//TODO:Implements Label referencing
			throw new RuntimeException("Kann das angegebene Feld nicht befüllen! " + field);
		}
		
		if(element.isDisplayed()){
			element.click();
		}else{
			if(browser instanceof JavascriptExecutor){
				((JavascriptExecutor)browser).executeScript("arguments[0].checked = true;", element);
			}else{
				element.click();
			}
		}
	}
	
	private boolean checkSpan(Field field){
		String expectedValue = field.getValue();
		String givenValue = null;
		
		if(field.getId() != null){
			givenValue = browser.findElement(By.xpath("//span[@id='" + field.getId() + "']"))
				.getText();
		}else if(field.getXpath() != null){
			givenValue = browser.findElement(By.xpath(field.getXpath()))
				.getText();
		}else{
			//TODO:Implements Label referencing
			return false;
		}
		
		field.set_value(givenValue);
		if(expectedValue == null)
			return givenValue == null || givenValue.equals("");

		return expectedValue.equals(givenValue);
	}
	
	private boolean checkInput(Field field){
		WebElement element = null;
		if(field.getId() != null){
			element = browser.findElement(By.xpath("//input[@id='" + field.getId() + "']"));
		}else if(field.getXpath() != null){
			element = browser.findElement(By.xpath(field.getXpath()));
		}else{
			//TODO:Implements Label referencing
			return false;
		}
		
		if(	"true".equals(field.getValue()) || 
			"false".equals(field.getValue())){
			
			Boolean shouldSelected = Boolean.parseBoolean(field.getValue());
			return shouldSelected == element.isSelected();
		}else{
			String givenValue = element.getAttribute("value");
			
			return field.getValue().equals(givenValue);
		}
	}
	
	private boolean checkTextarea(Field field){
		String expectedValue = field.getValue();
		String givenValue = null;
		
		if(field.getId() != null){
			givenValue = browser.findElement(By.xpath("//textarea[@id='" + field.getId() + "']"))
				.getAttribute("value");
		}else if(field.getXpath() != null){
			givenValue = browser.findElement(By.xpath(field.getXpath()))
				.getAttribute("value");
		}else{
			//TODO:Implements Label referencing
			return false;
		}
		
		field.set_value(givenValue);
		if(expectedValue == null)
			return givenValue == null || givenValue.equals("");

		return expectedValue.equals(givenValue);
	}
	
	private boolean checkSelect(Field field){
		String expectedValue = field.getValue();
		String givenValue = null;
		
		String xpath = null;
		if(field.getId() != null){
			xpath = "//select[@id='" + field.getId() + "']";
		}else if(field.getXpath() != null){
			xpath = field.getXpath();
		}else{
			//TODO:Implements Label referencing
			return false;
		}
		
		List<WebElement> options = browser.findElement(By.xpath(xpath))
										.findElements(By.tagName("option"));
		for(WebElement option : options){
			if(option.isSelected()){
				givenValue = option.getText();
				break;
			}
		}
		
		field.set_value(givenValue);
		if(expectedValue == null)
			return givenValue == null || givenValue.equals("");

		return expectedValue.equals(givenValue);
	}
}
