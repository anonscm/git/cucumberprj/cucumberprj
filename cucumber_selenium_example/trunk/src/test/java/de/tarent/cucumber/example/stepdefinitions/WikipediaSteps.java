package de.tarent.cucumber.example.stepdefinitions;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.annotation.de.Angenommen;
import cucumber.annotation.de.Dann;
import cucumber.annotation.de.Und;
import cucumber.annotation.de.Wenn;

public class WikipediaSteps {
	
	@Autowired
	private WebDriver browser;
	
	@Wenn("^(ich|) die Seite \"([^\"]*)\" öffne$")
	public void ich_die_Seite_oeffne(String t0, String url){
		browser.get(url);
	}
	
	@Und("^ich in das Eingabefeld mit der ID \"([^\"]*)\" den Wert \"([^\"]*)\" eingebe$")
	public void ich_in_das_Eingabefeld_mit_der_ID_den_Wert_eingebe(String inputID, String inputValue){
		WebElement element = browser.findElement(By.xpath("//input[@id='" + inputID + "']"));
		
		element.clear();
		element.sendKeys(inputValue);
	}
	
	@Und("^(ich |)auf den Button mit der ID \"([^\"]*)\" klicke$")
	public void ich_auf_klicke(String t0, String buttonId){
		browser.findElement(By.id(buttonId)).click();
	}
	
	@Dann("^erhalte ich einen Abschnitt mit dem Titel \"([^\"]*)\"$")
	public void erhalte_ich_einen_Abschnitt_mit_dem_Titel(String titel){
		assertTrue(browser.findElements(By.xpath("//span[contains(., '" + titel + "')]")).size() > 0);
	}
	
	@Angenommen("^[iI]ch bin auf der Seite \"([^\"]*)\"$")
	public void ich_bin_auf_der_Seite(String url){
		ich_die_Seite_oeffne(null, url);
	}
	
	@Dann("^löse ich einen Fehler aus$")
	public void loese_ich_einen_Fehler_aus(){
		fail("Ein Fehler hat zur Folge, dass ein Screenshot von der aktuell geladenen Seite erstellt und in den HTML-Report eingebettet wird!");
	}
}
