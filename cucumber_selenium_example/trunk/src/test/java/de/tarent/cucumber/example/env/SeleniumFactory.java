package de.tarent.cucumber.example.env;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import de.tarent.cucumber.example.env.event.HandlerStop;

public class SeleniumFactory implements HandlerStop {
	/**
	 * Alle benutzten WebDriver Instanzen müssen nach dem Beenden des
	 * Programmes wieder geschlossen werden.
	 */
	private static List<WebDriver> usedInstances = new ArrayList<WebDriver>();
	
	public SeleniumFactory(){
		//diese instanz soll benachrichtigt werden, wenn das Programm endet
		StartAndShutdownHook.addOnStopHandler(this);
	}
	
	public WebDriver buildLocalFF(){
		WebDriver instance = new FirefoxDriver();
		
		usedInstances.add(instance);
		
		return instance;
	}

	public WebDriver buildLocalHtmlUnit(){
		WebDriver instance = new HtmlUnitDriver(true);
		
		usedInstances.add(instance);
		
		return instance;
	}

	
	@Override
	public void handleStop() {
		//alle Instanzen müssen geschlossen werden, 
		//da sonst die Broswer nach Beendigung noch geöffnet bleiben
		for(WebDriver instance : usedInstances){
			instance.close();
		}
	}
}
